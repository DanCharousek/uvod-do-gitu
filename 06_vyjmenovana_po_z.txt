Harry měl pocit, že se z té čarodějnické jaz _ kovědy
brz _ čko zblázní. Stejně z něho bude bystrozor, a ne
jaz _ kovědec. Kdyby se tak každý jaz _ k dal rozpitvat
stejně snadno jako dračí jaz _ čky, které prodávali v
Medovém ráji. Z _ tra se ukáže. Bude muset vz _ vat
všechny kouzelnické svaté, aby se mu ta zkouška
vydařila. Dnes už je ale pozdě, navíc se udělala podezřelá
z _ ma. Místností prolétl průvan. Sušený z _ mostráz
stojící ve váze na stole se převrátil a zralé malvaz _ nky se skutálely na podlahu. V rohu místnosti cosi dopadlo na
zem. Harry z _ vl, vytáhl hůlku, aby z _ skal čas reagovat
na případného nepřítele.